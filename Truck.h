#include "Bulding.h"

class Truck {
    public:
        int x, y;
        int minX, maxX;
        int route;
        int capacity;
        bool isLoading;
        bool isReturnRide;
        bool init;

        int brick;
        int cement;
        int mineMaterial;
        
        int road;

        Truck(int min, int max, int type);
        void ride();
        void loading(Bulding * bulding);
        void unloading(Bulding * bulding);
        bool isStart();
        bool isEnd();
};

