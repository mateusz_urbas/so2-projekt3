#include "Bulding.h"
#include <math.h>
#include <unistd.h>

enum Type {
    brickFactory = 0,
    mine = 3,
};

Bulding::Bulding(int type) {
    buldingType = type;
    capacity = 1000;
    cement = 0;
    brick = 0;
    mineMaterial = 0;
}

void Bulding::Production() {
    if(buldingType == brickFactory){
        if(mineMaterial>4) {
            brick++;
            mineMaterial-=5;
        }
        usleep(300000);
    } 
    else {
        if(mineMaterial < 100) {
            mineMaterial++;
        }
        usleep(100000);
    }
}
