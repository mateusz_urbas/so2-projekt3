#include "Truck.h"
#include <math.h>
#include <unistd.h>

enum Type {
    brickFactory = 0,
    mine = 3,
};

Truck::Truck(int min, int max, int type) {
    maxX = max;
    minX = min + 10;
    x = min+10;
    y = 12;
    isReturnRide = false;
    isLoading = false;
    road = type;

    brick = 0;
    mineMaterial = 0;
}

void Truck::ride() {
    if(x < maxX && !isReturnRide && !isLoading) {
        x++;
    }
    else if (x > minX && isReturnRide && !isLoading) {
        x--;
    }

}

bool Truck::isStart() {
    if(x == minX){
        isReturnRide = !isReturnRide;
        y= 12;
        return true;
    }
    return false;
}

bool Truck::isEnd() {
    if(x == maxX){
        isReturnRide = !isReturnRide;
        return true;
    } 
    return false;
}

void Truck::loading(Bulding * bulding) {
    int x = 0;
    if(road == mine) {
        while(x < 20) {
            if(bulding->mineMaterial > 0) {
                bulding->mineMaterial -= 1;
                mineMaterial++;
                x++;
            }
            usleep(100000);
        }
    }
    else {
        while(x < 10) {
            if(bulding->brick > 0) {
                bulding->brick -= 1;
                brick++;
                x++;
            }
            usleep(500000);
        }
    }


}

void Truck::unloading(Bulding * bulding) {
    if(road == mine) {
        while(mineMaterial > 0) {
            bulding->mineMaterial += 1;
            mineMaterial--;
            usleep(100000);
        }
    }
    else {
        while(brick > 0) {
            bulding->brick += 1;
            brick--;
            usleep(700000);
        }   
    }
    
}

