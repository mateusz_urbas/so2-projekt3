#include "Truck.h"

#include <iostream>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <ncurses.h>
#include <unistd.h>
#include <pthread.h> 
#include <thread>
#include <math.h>
#include <vector>
#include <ctime>

using namespace std;
bool status = true;
std::vector < Truck* > Trucks;
vector < thread* > productionThread;

#define DELAY 30000

//conditional variable
pthread_mutex_t mutexMine = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condMineLoading = PTHREAD_COND_INITIALIZER;
bool mineLoading = false;

pthread_mutex_t mutexMine1 = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condMineUnloading = PTHREAD_COND_INITIALIZER;
bool mineUnloading = false;

//conditional variable
pthread_mutex_t mutexFactory = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condFactoryLoading = PTHREAD_COND_INITIALIZER;
bool factoryLoading = false;

pthread_mutex_t mutexFactory1 = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condFactoryUnloading = PTHREAD_COND_INITIALIZER;
bool factoryUnloading = false;

enum RouteBuldingType {
    brickFactory = 0,
    mine = 3,
    warehouse = 4
};

enum BudingPosition {
    minePosition = 10,
    brickFactoryPosition = 50,
    warehousePosition = 90,
};

Bulding* mineB;
Bulding* brickFactoryB;
Bulding* warehouseB;

//---------------------------------------------------------------------

void printBulding(int x, int type);
void initProduction();
void produceMaterial(Bulding * bulding);
void moveTruck(Truck * truck);
void endProgram();
//---------------------------------------------------------------------

void initProgram(vector<thread>& threads) {

    mineB = new Bulding(mine);
    brickFactoryB = new Bulding(brickFactory);
    warehouseB = new Bulding(warehouse);

    for (int i = 0; i < 3; i++)
    {
        Truck* truck = new Truck(minePosition, brickFactoryPosition, mine);
        Trucks.push_back(truck);
        threads.push_back(thread(moveTruck, truck));

        Truck* truck2 = new Truck(brickFactoryPosition, warehousePosition, brickFactory);
        Trucks.push_back(truck2);
        threads.push_back(thread(moveTruck, truck2));
        usleep(DELAY);
    }
}


int main(int argc, char *argv[]) {
    vector <thread> threads;
	initscr();
	noecho();
	curs_set(FALSE);
	refresh();

    initProgram(threads);
    
    // wątki do tworzenia materiałów
    thread generateMaterial = thread(produceMaterial, mineB);
    thread generateBrick = thread(produceMaterial, brickFactoryB);

    // wątek do zakonczenia programu
	thread pressButton = thread(endProgram);

	//Główny program, czyli wątek do wyświetlania
	while(status) {
		clear();
        printBulding(minePosition, mine);
        printBulding(brickFactoryPosition, brickFactory);
        printBulding(warehousePosition, warehouse);

		//Wyświetlaj aktualne pozycje
		for( int i = 0; i < Trucks.size(); i++ ) {
			mvprintw(Trucks[i] -> y, Trucks[i] -> x, "o");
			//printw("  x=%d, y=%d\n", Trucks[i] -> x, Trucks[i] -> y);
		}

		refresh();
		usleep(DELAY);
	}
 	endwin();
	pressButton.join();
    
    generateMaterial.join();
    generateBrick.join();

	std::cout<<"Zakończono program...\n";
}


void printBulding(int x, int type) {
    if(type == warehouse) {
        mvprintw(7,x, "||||||||||");
        mvprintw(8,x, "||||||||||");
        mvprintw(9,x, "||||||||||");
        mvprintw(10,x, "||||||||||");
        mvprintw(11,x, "||||||||||");
        mvprintw(12,x, "|||[ ]||||");

        mvprintw(15,x, "Cegly: "); printw("%d", warehouseB->brick);
    } else if(type == mine) {
        mvprintw(10,x, "||______||");
        mvprintw(10,x, "||__||__||");
        mvprintw(11,x, "||||||||||");
        mvprintw(12,x, "|||[ ]||||");

        mvprintw(14,x, "Glina: "); printw("%d", mineB->mineMaterial);

    } else {
        mvprintw(8,x, "______||__");
        mvprintw(9,x, "||||||||||");
        mvprintw(10,x, "| || || ||");
        mvprintw(11,x, "||||||||||");
        mvprintw(12,x, "|||[ ]||||");

        if(type == brickFactory) {
            mvprintw(14,x, "Cegly: "); printw("%d", brickFactoryB->brick);
            mvprintw(15,x, "Glina: "); printw("%d", brickFactoryB->mineMaterial);
        }
    }
}

//---------------------------------------------------------------------
void produceMaterial(Bulding * bulding) {
    while(status){
        bulding -> Production();
    }
}
//---------------------------------------------------------------------

void mine_loading(Truck * truck) {
    pthread_mutex_lock(&mutexMine);
        while(mineLoading) {
            pthread_cond_wait(&condMineLoading, &mutexMine); 
        }
        truck -> y = 12;
        mineLoading = true;
        truck->loading(mineB);
        mineLoading = false;
        pthread_cond_broadcast(&condMineLoading);
    pthread_mutex_unlock(&mutexMine);
}

void mine_unloading(Truck * truck) {
    pthread_mutex_lock(&mutexMine1);
        while(mineUnloading) {
            pthread_cond_wait(&condMineUnloading, &mutexMine1); 
        }
        truck -> y = 11;
        mineUnloading = true;
        truck->unloading(brickFactoryB);
        mineUnloading = false;
        pthread_cond_broadcast(&condMineUnloading);
    pthread_mutex_unlock(&mutexMine1);
}

void factory_loading(Truck * truck) {
    pthread_mutex_lock(&mutexFactory);
        while(factoryLoading) {
            pthread_cond_wait(&condFactoryLoading, &mutexFactory); 
        }
        truck -> y = 12;
        factoryLoading = true;
        truck->loading(brickFactoryB);
        factoryLoading = false;
        pthread_cond_broadcast(&condFactoryLoading);
    pthread_mutex_unlock(&mutexFactory);
}

void factory_unloading(Truck * truck) {
    pthread_mutex_lock(&mutexFactory1);
        while(factoryUnloading) {
            pthread_cond_wait(&condFactoryUnloading, &mutexFactory1); 
        }
        truck -> y = 11;
        factoryUnloading = true;
        truck->unloading(warehouseB);
        factoryUnloading = false;
        pthread_cond_broadcast(&condFactoryUnloading);
    pthread_mutex_unlock(&mutexFactory1);
}

void moveTruck(Truck * truck) {
    if(truck->road == mine) mine_loading(truck);
    else factory_loading(truck);

    while(status){
        truck->ride();
        if(truck->isStart()) {
            if(truck->road == mine) mine_loading(truck);
            else factory_loading(truck);
        }
        if(truck->isEnd()) {
            if(truck->road == mine) mine_unloading(truck);
            else factory_unloading(truck);
        }
        usleep(50000);
    }
}

void endProgram() {
	unsigned char znak;
	while( znak != 113 ) { //znak q
		znak = getch();
	};
	status = false;
}
